ZvukExDoIn
	- předání hodnoty hlasitosti pro exteriérové zvuky, mimo loko vždy 1, uvnitř loko math.max(OknoL, OknoP)
	
LoopZvukEx
	- předání hodnoty hlasitosti pro loopované zvuky, aby snad fungovaly ex i in loopy správně
	- 0 mimo loko, 1 uvnitř loko
	
Ventilatory
	- otáčky ventilátorů (může být klidně 0-1, spíše ale větší rozsah, třeba 0-100), dle toho se bude dát navázat více variant zvuků větráků, aby se nemusela jen jedna volnoběhová nahrávka zvyšovat pitchem, ale celé se to dalo lépe regulovat
	
VentilatoryEDB
	- stejné jak Ventilatory
	
ProudTM
	- math.max(proudTM1, proudTM2)