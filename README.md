# ZSCS131 for Train Simulator

ZSCS131 is an fully scripted loco for Train Simulator 2020 (RailWorks).

## Installation

Download the latest builded version in [releases](https://gitlab.com/JachyHm/ZSCS131/-/releases) and unpack it in game, or download source and buld it yourself using BlueprintEditor.exe.

## Contributing
Pull requests and bugfixes are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to test everything before pushing to Git.

## License
[MIT](https://gitlab.com/JachyHm/ZSCS131/-/blob/master/LICENSE)